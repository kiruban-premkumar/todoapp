# Install ruby 2.4.3 & rails 5

```
rvm install 2.4.3
rvm default 2.4.3
rvm use 2.4.3
gem install rails
```

# check

```
ruby -v
rails -v
```

# Generate the app

```
rails new todoapp
```

# Start the server

```
rails s -b $IP -p $PORT
```

# How to generate a DB migration file

```
rails g migration create_todos
```

# How to run migraiton

```
rails db:migrate
```

# How to interact with DB

```
rails console
```

# How to create a model

```
class Todo < ApplicationRecord
	
end
```

# How to validate attributes in model

```ruby
class Todo < ApplicationRecord
	validates :name, presence: true
end
```

# How to add cruds routes to a ressource

```ruby
resources :todos
```

# Example of a controller

```ruby
class TodosController < ApplicationController
	def new
		@todo = Todo.new
	end
	
	def create
		@todo = Todo.new(todo_params)
		if @todo.save
			flash[:notice] = "Todo added successfuly!"
			redirect_to todo_path(@todo)
		else
			render 'new'
		end
	end
	
	def show
		@todo = Todo.find(params[:id])
	end
	
	private
	
	def todo_params
		params.require(:todo).permit(:name, :description)
	end
end
```
